# Cognito Idp Example


## Running in virtualenv

```bash
joseba@LIFE-A:~/src/macc/$git clone https://gitlab.com/macc_ci_cd/tdd-clean-example.git
joseba@LIFE-A:~/src/macc/$cd cognito-idp-example
joseba@LIFE-A:~/src/macc/cognito-idp-example$mkdir uploads
joseba@LIFE-A:~/src/macc/cognito-idp-example$ virtualenv -p python3 venv
joseba@LIFE-A:~/src/macc/cognito-idp-example$ source venv/bin activate
joseba@LIFE-A:~/src/macc/cognito-idp-example$pip install -r requirements.txt
joseba@LIFE-A:~/src/macc/cognito-idp-example$ cd project
joseba@LIFE-A:~/src/macc/cognito-idp-example$ python server.py 
```


## Migrations
```bash
alembic init .
PYTHONPATH=.. alembic  revision --autogenerate -m "Froga1- Aldaketarik gabe"
alembic upgrade head
alembic history 
alembic downgrade base
alembic stamp head (para actualizar/sync)
```
