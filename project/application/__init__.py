from flask import Flask
from flask_login import LoginManager
from flask_bootstrap import Bootstrap

# TODO: HAU GALERIARAKO DA MOMENTUZ GERO KENDUKO DUGU


from .controller.login_local import config

# ToDo : Holan egin behar da DB-ko dana
from main.base import Session


def create_app() -> Flask:
    app = Flask(__name__)
    app.secret_key = config.FLASK_SECRET
    app.config["SECRET_KEY"] = "SECRET_KEY"
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
    app.config["UPLOAD_FOLDER"] = "static/uploads/"

    Bootstrap(app)

    from .controller.login_local.routes import blueprint as blueprint_login_local

    from .controller.storage_blueprint.storage import blueprint_storage


    with app.app_context():
        from application.controller import index
        from users.infrastructure.sqlalchemy_users_respository import SqlAlchemyUsersRepository


    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(user_id):
        # def load_user(user_id):
        # since the user_id is just the primary key of our user table, use it in the query for the user
        try:
            user_repository = SqlAlchemyUsersRepository()
            id = int(user_id)
            user = user_repository.get(int(id))
            return user
        except Exception as e:
            return None

    app.register_blueprint(blueprint_login_local, url_prefix='/auth')
    app.register_blueprint(blueprint_storage, url_prefix='/storage')

    @app.teardown_appcontext
    def cleanup(resp_or_exc):
        Session.remove()

    return app
