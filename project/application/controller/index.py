from flask import current_app, url_for, redirect


@current_app.route('/')
def index():
    return redirect(url_for('auth.index'))
