import dataclasses
from dataclasses import dataclass

from datetime import datetime
import requests

import boto3
from boto3 import Session
import botocore
from jose import jwt

from flask import session

from application.service import util
from . import config

cognito_client = boto3.client('cognito-identity')

JWKS_URL = ("https://cognito-idp.%s.amazonaws.com/%s/.well-known/jwks.json"
            % (config.AWS_REGION, config.COGNITO_POOL_ID))
resp = requests.get(JWKS_URL)
JWKS = resp.json()["keys"]


def verify(token, access_token=None):
    """Verify a cognito JWT"""
    # get the key id from the header, locate it in the cognito keys
    # and verify the key
    header = jwt.get_unverified_header(token)
    key = [k for k in JWKS if k["kid"] == header['kid']][0]
    id_token = jwt.decode(token, key, audience=config.COGNITO_CLIENT_ID, access_token=access_token)
    return id_token


def refresh_token(refresh_token_value):
    idp_client = boto3.client('cognito-idp', region_name=config.AWS_REGION)
    try:
        response_refresh = idp_client.initiate_auth(
            ClientId=config.COGNITO_CLIENT_ID,
            AuthFlow='REFRESH_TOKEN_AUTH',
            AuthParameters={
                'REFRESH_TOKEN': refresh_token_value
                # Note that SECRET_HASH is missing from JSDK
                # Note also that DEVICE_KEY is missing from my example
            })
        ret = response_refresh

    except botocore.exceptions.ClientError as e:
        error = e.response
        ret = error

    return ret


@dataclass
class AwsCredentials:
    aws_access_key_id: str
    aws_secret_access_key: str
    aws_session_token: str
    name: str
    email: str


def get_cognito_login_url():
    session['csrf_state'] = util.random_hex_bytes(8)
    cognito_login = ("https://%s/"
                     "login?response_type=code&client_id=%s"
                     "&state=%s"
                     "&redirect_uri=%s/auth/callback" %
                     (config.COGNITO_DOMAIN, config.COGNITO_CLIENT_ID, session['csrf_state'],
                      config.BASE_URL))
    return cognito_login


def get_cognito_logout_url():
    cognito_logout = ("https://%s/"
                      "logout?response_type=code&client_id=%s"
                      "&logout_uri=%s/" %
                      (config.COGNITO_DOMAIN, config.COGNITO_CLIENT_ID, config.BASE_URL))
    return cognito_logout


def get_cognito_temporal_role(token):
    request_parameters = {'grant_type': 'authorization_code',
                          'client_id': config.COGNITO_CLIENT_ID,
                          'code': token,
                          "redirect_uri": config.BASE_URL + "/auth/callback"}
    response = requests.post("https://%s/oauth2/token" % config.COGNITO_DOMAIN,
                             data=request_parameters)
    if response.status_code == requests.codes.ok:  # and csrf_state == session['csrf_state']:
        verify(response.json()["access_token"])
        id_token = verify(response.json()["id_token"], response.json()["access_token"])
        id_token_value = response.json()["id_token"]
        identity_client = boto3.client('cognito-identity')

        expire = id_token["exp"]
        expires = datetime.utcfromtimestamp(expire)
        now = datetime.utcnow()
        expires_seconds = (expires - now).total_seconds()
        if expires_seconds < 0:
            response_refresh = refresh_token(response.json()["refresh_token"])
            id_token = response_refresh['AuthenticationResult']['IdToken']

        id_response = identity_client.get_id(
            IdentityPoolId='us-east-1:55d720e9-6e22-42c4-805c-cdfe8ee80d42',
            Logins={
                'cognito-idp.us-east-1.amazonaws.com/us-east-1_8oNCGlyNe': id_token_value
            }
        )

        cognito_response = identity_client.get_credentials_for_identity(
            IdentityId=id_response['IdentityId'],
            Logins={
                'cognito-idp.us-east-1.amazonaws.com/us-east-1_8oNCGlyNe': id_token_value
            })

        access_key = cognito_response['Credentials']['AccessKeyId']
        secret_key = cognito_response['Credentials']['SecretKey']
        session_key = cognito_response['Credentials']['SessionToken']

        name = id_token["cognito:username"]
        email = id_token["email"]

        aws_credentials = AwsCredentials(
            access_key,
            secret_key,
            session_key,
            name,
            email
        )

        return aws_credentials
    else:
        return None


class AwsAccesser:
    def __init__(self, aws_account_id, identity_pool_id, provider_name):
        self._aws_account_id = aws_account_id
        self._identity_pool_id = identity_pool_id
        self.provider_name = provider_name

    def get_credentials(self, id_token, role_arn=None):
        id_response = cognito_client.get_id(
            # AccountId=self._aws_account_id,
            IdentityPoolId=self._identity_pool_id,
            Logins={self.provider_name: id_token}
        )
        # TODO handle any error

        request = {
            'IdentityId': id_response['IdentityId'],
            'Logins': {self.provider_name: id_token}
        }
        if role_arn:
            request['CustomRoleArn'] = role_arn

        credentials_response = cognito_client.get_credentials_for_identity(**request)
        # TODO handle any error

        return AwsCredentials(
            credentials_response['Credentials']['AccessKeyId'],
            credentials_response['Credentials']['SecretKey'],
            credentials_response['Credentials']['SessionToken']
        )

    def get_boto3_session(self, id_token, role_arn=None):
        aws_credentials = self.get_credentials(id_token, role_arn)
        return Session(aws_access_key_id=aws_credentials.aws_access_key_id ,
                       aws_secret_access_key=aws_credentials.aws_secret_access_key,
                       aws_session_token=aws_credentials.aws_session_token)
        #return Session(**dataclasses.asdict(aws_credentials))
