
import os

# PHOTOS_BUCKET = os.environ['PHOTOS_BUCKET']
# FLASK_SECRET = os.environ['FLASK_SECRET']
FLASK_SECRET = 'super secret key'
# DATABASE_HOST = os.environ['DATABASE_HOST']
# DATABASE_USER = os.environ['DATABASE_USER']
# DATABASE_PASSWORD = os.environ['DATABASE_PASSWORD']
# DATABASE_DB_NAME = os.environ['DATABASE_DB_NAME']

AWS_REGION = "us-east-1"
# COGNITO_POOL_ID = os.environ['COGNITO_POOL_ID']
COGNITO_POOL_ID = 'us-east-1_8oNCGlyNe' # 'us-east-1:55d720e9-6e22-42c4-805c-cdfe8ee80d42'
# COGNITO_CLIENT_ID = os.environ['COGNITO_CLIENT_ID']
COGNITO_CLIENT_ID = '2ilcm2pr8fdknkasm49021ta9r'
# COGNITO_CLIENT_SECRET = os.environ['COGNITO_CLIENT_SECRET']
# COGNITO_DOMAIN = os.environ['COGNIT•••••••••••••O_DOMAIN']
COGNITO_DOMAIN = 'froga.auth.us-east-1.amazoncognito.com'


# BASE_URL = os.environ['BASE_URL']
BASE_URL = 'http://localhost:15000'

S3_BUCKET_NAME = 'froga-joseba'  # 'froga-jaagirre'
S3_PREFIX_PER_ROLE = {
    'arn:aws:iam::abc123:role/Role1': 'Folder1/',
    'arn:aws:iam::abc123:role/Role2': 'Folder2/'
}