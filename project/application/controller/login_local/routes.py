from flask import Blueprint, render_template, url_for, request, redirect, flash, session, render_template_string
from flask_login import login_user, login_required, current_user, logout_user
import flask_login

from werkzeug.security import check_password_hash

from .aws_credentials import get_cognito_login_url, get_cognito_temporal_role, get_cognito_logout_url

# from application import db  # ToDo
from main.base import Session as dbSession
from users.domain.user import User as UserEntity
from users.infrastructure.sqlalchemy_users_respository import SqlAlchemyUsersRepository

blueprint = Blueprint('auth', __name__, template_folder='./templates')


@blueprint.route('/')
def index():
    return render_template('index_login.html')


@blueprint.route('/profile')
@login_required
def profile():
    name = current_user.name
    return render_template('profile.html', name=name)


@blueprint.route('/login')
def login():
    cognito_login = get_cognito_login_url()
    return redirect(cognito_login)
    # return render_template('login.html')


@blueprint.route('/signup')
def signup():
    cognito_login = get_cognito_login_url()
    return redirect(cognito_login)
    # return render_template('signup.html')


@blueprint.route('/callback')
def login_post():
    code = request.args.get('code')
    aws_credentials = get_cognito_temporal_role(code)

    if aws_credentials:
        user_repository = SqlAlchemyUsersRepository()
        user = user_repository.getByName(aws_credentials.name)

        if user is None:  # if a user is found, we want to redirect back to signup page so user can try again
            new_user = UserEntity(id=0, email=aws_credentials.email, name=aws_credentials.name,
                                  password ='not required', aws_access_key_id='not_required',
                                  aws_secret_access_key='not_required', aws_session_token='not_required')
            user_repository.save(new_user)
            user = new_user
            # user.is_active = True
            #dbSession.commit()
            user = user_repository.getByName(aws_credentials.name)

        user.aws_access_key_id = aws_credentials.aws_access_key_id
        user.aws_secret_access_key = aws_credentials.aws_secret_access_key
        user.aws_session_token = aws_credentials.aws_session_token
        dbSession.commit()

        flask_login.login_user(user)  # ToDO implement logout

        return redirect(url_for('auth.profile'))

    return render_template_string("""
               {% extends "base.html" %}
               {% block content %}
                   <p>Something went wrong</p>
               {% endblock %}""")


@blueprint.route('/logout')
@login_required
def logout():
    logout_user()
    cognito_logout = get_cognito_logout_url()
    return redirect(cognito_logout)


@blueprint.route('/login', methods=['POST'])
def login_local_post():
    # login code goes here
    email = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False

    user_repository = SqlAlchemyUsersRepository()
    user = user_repository.getByMail(email=email)

    # check if the user actually exists
    # take the user-supplied password, hash it, and compare it to the hashed password in the database
    if not user or not check_password_hash(user.password, password):
        flash('Please check your login details and try again.')
        return redirect(url_for('auth.login'))  # if the user doesn't exist or password
    # if the above check passes, then we know the user has the right credentials
    login_user(user, remember=remember)
    return redirect(url_for('auth.profile'))


@blueprint.route('/signup', methods=['POST'])
def signup_post():
    # code to validate and add user to database goes here
    email = request.form.get('email')
    name = request.form.get('name')
    password = request.form.get('password')
    user_repository = SqlAlchemyUsersRepository()
    user = user_repository.getByMail(email=email)

    if user:  # if a user is found, we want to redirect back to signup page so user can try again
        flash('Email address already exists')
        return redirect(url_for('auth.signup'))

    new_user = UserEntity(id=0, email=email, name=name, password=password)
    user_repository.save(new_user)
    # add the new user to the database
    user_repository.save(new_user)

    return redirect(url_for('auth.login'))
