import dataclasses
import os

from flask_login import login_user, login_required, current_user, logout_user
from flask import Blueprint, render_template, request, redirect, url_for, session, flash
import botocore

from werkzeug.utils import secure_filename

from application.controller.login_local.aws_credentials import AwsCredentials
from application.service.storage import upload_file, show_image, upload_file_with_session
import boto3
from users.infrastructure.sqlalchemy_users_respository import SqlAlchemyUsersRepository

# from application.model.models import User

blueprint_storage = Blueprint('storage', __name__, template_folder='templates')

UPLOAD_FOLDER = "../uploads"
# BUCKET = "jaagirre-academy-image-data"
BUCKET = 'froga-jaagirre'


@blueprint_storage.route("/")
@login_required
def home():
    return render_template('index.html')


@blueprint_storage.route("/ok")
@login_required
def ok():
    return render_template('ok.html')


@blueprint_storage.route("/error")
@login_required
def error():
    return render_template('error.html')


@blueprint_storage.route("/upload", methods=['POST'])
@login_required
def upload():
    boto_session = set_aws_session()

    if request.method == "POST":
        try:
            f = request.files['file']
            local_file_name = os.path.join(UPLOAD_FOLDER, secure_filename(f.filename))
            f.save(local_file_name)
            try:
                aws_credentials = AwsCredentials(
                    current_user.aws_access_key_id,
                    current_user.aws_secret_access_key,
                    current_user.aws_session_token,
                    '',
                    ''
                )
                # credentials = dataclasses.asdict(aws_credentials)
                upload_file_with_session(aws_credentials, local_file_name, f"uploads/{f.filename}", BUCKET)
                return redirect(url_for('storage.ok'))
            except  botocore.exceptions.ClientError as error:
                flash('You don\'t have access')
                return redirect(url_for("storage.error"))
        except FileNotFoundError as e:
            return redirect(url_for('storage.error'))


@blueprint_storage.route("/pics")
@login_required
def list():
    aws_credentials = AwsCredentials(
        current_user.aws_access_key_id,
        current_user.aws_secret_access_key,
        current_user.aws_session_token,
        current_user.name,
        current_user.email
    )
    contents = show_image(BUCKET, aws_credentials)
    return render_template('collection.html', contents=contents)


def set_aws_session():
    aws_credentials = AwsCredentials(
        current_user.aws_access_key_id,
        current_user.aws_secret_access_key,
        current_user.aws_session_token,
        '',
        ''
    )

    # credentials = dataclasses.asdict(aws_credentials)

    boto_session = boto3.Session(aws_access_key_id=aws_credentials.aws_access_key_id,
                                 aws_secret_access_key=aws_credentials.aws_secret_access_key,
                                 aws_session_token=aws_credentials.aws_session_token)
    return boto_session
