import boto3


def upload_file(local_file_name, file_name, bucket_name):
    s3_client = boto3.client('s3')
    response = s3_client.upload_file(local_file_name, bucket_name, file_name)
    return response


def upload_file_with_session(credentials, local_file_name, file_name, bucket_name):
    # s3_client = session.resource('s3')
    s3_client = boto3.client('s3', aws_access_key_id=credentials.aws_access_key_id,
                             aws_secret_access_key=credentials.aws_secret_access_key,
                             aws_session_token=credentials.aws_session_token)

    response = s3_client.upload_file(local_file_name, bucket_name, file_name)
    return response




def show_image(bucket_name, credentials=None):
    # s3_client = boto3.client('s3')
    s3_client = boto3.client('s3', aws_access_key_id=credentials.aws_access_key_id,
                             aws_secret_access_key=credentials.aws_secret_access_key,
                             aws_session_token=credentials.aws_session_token)
    public_urls = []
    try:
        for item in s3_client.list_objects(Bucket=bucket_name)['Contents']:
            presigned_url = s3_client.generate_presigned_url('get_object',
                                                             Params={
                                                                 'Bucket': bucket_name,
                                                                 'Key': item['Key']}
                                                             , ExpiresIn=100)
            public_urls.append(presigned_url)
    except Exception as e:
        pass
    return public_urls
