# coding=utf-8

import os

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session

# engine = create_engine('postgresql://usr:pass@localhost:5432/sqlalchemy')
# engine = create_engine(os.environ['SQLALCHEMY_URL'])
engine = create_engine('sqlite:///db.sqlite')


# Session = sessionmaker(bind=engine) => Scoped_session mira si ya hay un hilo y en ese caso no lo crea , sino si
Session = scoped_session(sessionmaker(bind=engine))

# TODO: Use same db session in differentes packages infrastructure
Base = declarative_base()


# from posts.infrastructure.Posts import Post, Comment
from users.infrastructure.sqlalchemy_users_respository import UserModel


