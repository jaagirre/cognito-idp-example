from application import create_app
from main.base import Base, engine
# from application import db
app = create_app()

if __name__ == '__main__':

    with app.app_context():
        # db.create_all()
        Base.metadata.create_all(engine)
    app.run(host='0.0.0.0', port=15000, debug=True)
