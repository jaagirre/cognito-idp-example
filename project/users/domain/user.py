from dataclasses import dataclass

UserId = int


@dataclass
class User:
    id: UserId
    name: str
    email: str
    password: str
    aws_access_key_id: str
    aws_secret_access_key: str
    aws_session_token: str

    def login(self) -> None:
        pass

    def logout(self) -> None:
        pass
