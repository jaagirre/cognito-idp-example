import abc
from users.domain.user import User


class UsersRepository(abc.ABC):
    @abc.abstractmethod
    def get(self, user_id: str) -> User:
        pass

    @abc.abstractmethod
    def save(self, user) -> None:
        pass
