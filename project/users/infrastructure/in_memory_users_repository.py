import copy
from typing import Dict

from users.domain.users import UsersRepository
from users.domain.user import User, UserId


class InMemoryUsersRepository(UsersRepository):

    def __init__(self) -> None:
        self._storage: Dict[UserId, User] = {}

    def get(self, user_id: str) -> User:
        return copy.deepcopy(self._storage[user_id])

    def save(self, user: User) -> None:
        self._storage[user.id] = copy.deepcopy(user)
