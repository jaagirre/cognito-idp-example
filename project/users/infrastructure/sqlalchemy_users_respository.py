# domain
from users.domain.users import UsersRepository
from users.domain.user import User

# infrastructutre mix with  application level ToDo refcator Aqui estamos rompiendo la direccion del hexagono. ToDo
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash


from flask_login import UserMixin
from sqlalchemy.sql import func

from main.base import Base
from main.base import Session as dbSession
from sqlalchemy import Column, Integer, String, Text, ForeignKey, Table, DateTime, Boolean
from sqlalchemy.orm import relationship

from flask_login import UserMixin

class SqlAlchemyUsersRepository(UsersRepository):

    def __init__(self):
        pass # hemen be zeozer egin beharko zan

    def get(self, user_id: int) -> User:
        return dbSession.query(UserModel).filter(UserModel.id == user_id).first()

    def getByMail(self, email: str) -> User:
        return dbSession.query(UserModel).filter(UserModel.email == email).first()

    def getByName(self, name: str) -> User:
        return dbSession.query(UserModel).filter(UserModel.name == name).first()

    def save(self, user_data: User) -> None:

        user = dbSession.query(UserModel).filter(UserModel.email == user_data.email).first()  # if this returns a user, then the email already exists in database

        if user is None:  # if a user is found, we want to redirect back to signup page so user can try again
            # create a new user with the form data. Hash the password so the plaintext version isn't saved.
            new_user = UserModel(email=user_data.email, name=user_data.name, password=generate_password_hash(user_data.password, method='sha256'))
            # add the new user to the database
            dbSession.add(new_user)
            dbSession.commit()


class UserModel( Base, UserMixin):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)  # primary keys are required by SQLAlchemy
    id_token = Column(String(100))
    email = Column(String(100), unique=True)
    password = Column(String(100))
    name = Column(String(1000))
    aws_access_key_id = Column(String(1000))
    aws_secret_access_key = Column(String(1000))
    aws_session_token = Column(String(1000))
    created_data = Column(DateTime, default=func.now(), nullable=False)


